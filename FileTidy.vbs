' ****************************************************************
' GENERIC FileTidy.VBS
'
'     Script for Deleting Files in the Directory
' 
' Arguments in order (0) Path
'                    (1) Explicit File Extension
'		     
' ****************************************************************

' **** MAIN ****
	
' **** VARIABLES ****

	dim fso, WshShell, sPath, sFileext
	

' **** PREPARATION ****
	
	Set colArgs = WScript.Arguments
	if colArgs.length <2 then
		msgbox "Syntax:- FileTidy.vbs path extention"
		wscript.quit
	end if
	
	'msgbox colArgs.length
	
	sPath = colArgs(0)
	sFileext = colArgs(1)
		
	Set WshShell = WScript.CreateObject("WScript.Shell")
        Set fso = CreateObject("Scripting.FileSystemObject")	        


' **** PROCESSING ****
'msgbox sFileext
	if sFileext <> "*" then
		delfile(sPath & "*" & sFileext)
	
		
	End if	
		
' **** END MAIN **** 



Private Sub delfile(sFilename)
		
		'msgbox (sFilename)
 
        	'If fso.FileExists(sFilename) Then
    	    		'                     Response = MsgBox (sFilename)
    	    	'	fso.DeleteFile(sFilename)
    	    	'	msgbox(sFilename)
	        'End if
on error resume next
fso.DeleteFile(sFilename)
if err.number > 0 then
	'msgbox("Error Number: " & err.number & " - There are no files to delete")
	on error goto 0 
end if
End Sub
