option explicit


'**************************************************************************
' 		 GENERIC COPYRENAMEFILE.VBS
' 	
' Script for copying files from one directory to another
'
' Arguments in order (0) Source Path
'		     		 (1) Destination Path
'		             (2) sfName
'		             (3) dfName
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, WshShell, sPath, dPath, sfName, dfName, numArgs, sMonth, sDay, sYear, lYear, sCustomName, sDate, dDate
	Dim strFileName, HText, HNewText, FText, FNewText, strText, strNewText, objFile  
'***** PREPERATION *****


	numArgs = WScript.Arguments.Count

	sPath = WScript.Arguments.Item(0)
	sDate = WScript.Arguments.Item(1)
	sfName = WScript.Arguments.Item(2)
	dPath = WScript.Arguments.Item(3)
	dDate = WScript.Arguments.Item(4)
	dfName = WScript.Arguments.Item(5)
	

	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set fso = CreateObject("Scripting.FileSystemObject")




'***** PROCESSING *****


		
		
		
	'-- Create Date Function of current date  
 if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  'sYear = Year(Now)
  lYear = mid(Year(Now),1,4)
  sYear = mid(Year(Now),3,4)
					  
		  '-- Create the Custom filename
		 
		
'		    sCustomName = sYear & sMonth & sDay
'	 MsgBox sCustomName
			
		
	sDate = replace(sDate,"YYYY",lYear)
	sDate = replace(sDate,"YY",sYear)
	sDate = replace(sDate,"MM",sMonth)
	sDate = replace(sDate,"DD",sDay)

    dDate = replace(dDate,"YYYY",lYear)
	dDate = replace(dDate,"YY",sYear)
	dDate = replace(dDate,"MM",sMonth)
	dDate = replace(dDate,"DD",sDay)


	'wscript.echo sPath & sCustomName & sfName ,  dPath & dfName

		'MsgBox sPath & "AL" & sDate & sfName
		'MsgBox dPath & dDate & "_" & dfName
		'Function FileExists
			
			If (fso.FileExists(sPath & "AL" & sDate & sfName)) Then
		      'msg = filespec & " exists."
		      fso.CopyFile sPath & "AL" & sDate & sfName, dPath & dDate & "_" & dfName
		      'fso.CopyFile sPath & "AL" & sDate & sfName, dPath & dDate & "_" & dfName
		      
		      
		    Const ForReading = 1

			Const ForWriting = 2
			
			
			strFileName = dPath & dDate & "_" & dfName
			'MsgBox strFileName
			
			HText = "EDI_902_LOOKUP_" & sDate
			'MsgBox strOldText
			
			HNewText = "LOOKUP_" & dDate
			'MsgBox strNewText
			
			FText = "EDI_ENDOFFILE"
			'MsgBox strOldText
			
			FNewText = "ENDOFFILE"
			'MsgBox strNewText
			
			
			'Set objFSO = CreateObject("Scripting.FileSystemObject")
			
			Set objFile = fso.OpenTextFile(strFileName, ForReading)
			
			strText = objFile.ReadAll
			
			objFile.Close
			
			
			'strNewText = Replace(strText, strOldText, strNewText)
			strText = Replace(strText , HText, HNewText)
			
			strText = Replace(strText , FText, FNewText)
			
			Set objFile = fso.OpenTextFile(strFileName, ForWriting)
			
			objFile.WriteLine strText
			'objFile.WriteLine FNewText
			
			
			objFile.Close
			

		      
		      
		      
		      
		      
		      
		      Else 
		      
		      MsgBox "902 Lookup file is not available!"
		   'ElseIf (fso.FileExists(sPath & sfName)) Then
		      'msg = filespec & " doesn't exist."
		      'fso.CopyFile sPath & sfName, dPath & sCustomName & "_" & dfName
		   End If
		   'ReportFileStatus = msg
		'End Function
	
	
	
	
	'Function FileExists(sfName)

	'fso.CopyFile sPath & sCustomName & sfName ,  dPath & sCustomName & "_" & dfName
	
	'End Function
	
'***** END MAIN *****